(ns jdbctest.core)

(require '[clojure.java.jdbc :as j]
         '[jdbctest.counter :as counter]
         '[jdbctest.emailer :as emailer]
         '[postal.core :refer [send-message]])


(def mysql-db {:subprotocol "mysql"
               :subname "//43.255.154.95:3306/sales_emailers"
               :user "ro_admin"
               :password "#Tether@12345"})

(defn getEmails[]
  (for [emails (filter #(nil? (:email_sent_at % ) ) (j/query mysql-db ["select * from email_marketing_test where email_sent_at IS NULL LIMIT 0 , 100000"]))]
    (emailer/emailer (emails :full_name) (emails :id) (emails :email) )))



(defn -main
  "I don't do a whole lot."
  [& args]
  (println (for [status (getEmails)]
             (get status :message)))
  )
